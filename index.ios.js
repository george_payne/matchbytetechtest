// @flow

import {
  AppRegistry,
} from 'react-native';

import MatchbyteTechTest from './src/MatchbyteTechTest';

AppRegistry.registerComponent('matchbyteTechTest', () => MatchbyteTechTest);
