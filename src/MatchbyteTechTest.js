// @flow

import React from 'react';
import { KeyboardAvoidingView } from 'react-native';
import styled from 'styled-components/native';
import colors from 'constants/colors';
import { CheckIn } from 'organisms';

const Container = styled(KeyboardAvoidingView)`
        flex: 1;
        flex-direction: row;
        justify-content: center;
        align-items: center;
        background-color: ${colors.offWhite};
        padding: 6;
      `,
      MatchbyteTechTest = () => (
        <Container
          behavior={'height'}
        >
          <CheckIn />
        </Container>
      );

export default MatchbyteTechTest;
