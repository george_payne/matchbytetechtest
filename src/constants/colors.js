// @flow

const colors = {
  white: '#FFFFFF',
  offWhite: '#F5FCFF',
  darkGray: '#1F201E',
  lightGray: '#CACACA',
  shadowGray: '#E6E6E6',
  red: '#CC0000',
};

export default colors;
