// @flow

import React from 'react';
import { Alert } from 'react-native';
import styled from 'styled-components/native';
import { ValidatedForm } from 'molecules';
import colors from 'constants/colors';

const Container = styled.View`
        flex: 1;
        border-width: 1;
        border-style: solid;
        border-color: ${colors.shadowGray};
        background-color: ${colors.white};
        border-radius: 2;
        shadow-color: ${colors.darkGray};
        shadow-offset: 5 10;
        shadow-opacity: 0.8;
        shadow-radius: 2;
        elevation: 3;
        padding: 4;
      `,
      Row = styled.View`
        flex-direction: row;
        justify-content: space-between;
        align-items: center;
        padding: 4;
      `,
      BasicText = styled.Text`
        flex: 1;
        color: ${colors.darkGray};
        text-align: ${({ center }) => center ? 'center' : 'left'};
        line-height: 22;
      `,
      TitleText = styled.Text`
        color: ${colors.darkGray};
        text-align: center;
        font-weight: bold;
        font-size: 18;
      `,
      FlyBoys = styled.Image`
        width: 40;
        height: 40;
      `,
      CheckIn = () => (
        <Container>
          <Row>
            <BasicText center>
              {'Log-in now to view your upcoming and past flights'}
            </BasicText>
          </Row>
          <Row>
            <FlyBoys source={require('assets/flyBoys_1.png')} />
            <TitleText>{'Cut the queue. Check in now.'}</TitleText>
            <FlyBoys source={require('assets/flyBoys_2.png')} />
          </Row>
          <Row>
            <BasicText>
              {'Check in online between 72 hours and 1 hour before departure. Head to the airport and get outta here. It\'s that simple!'}
            </BasicText>
          </Row>
          <ValidatedForm
            submit={(values) => {
              Alert.alert(
                'Form Values',
                JSON.stringify(values, null, 2),
                [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
                { cancelable: false },
              );
            }}
          >
            <ValidatedForm.Row>
              <ValidatedForm.Input
                name={'ref'}
                placeholder={'Booking Reference'}
                autoCapitalize={'characters'}
                validator={/^[A-z0-9]+$/}
                length={6}
              />
              <ValidatedForm.Input
                name={'lastname'}
                placeholder={'Last name'}
                autoCapitalize={'words'}
                length={l => (l > 3)}
              />
            </ValidatedForm.Row>
            <ValidatedForm.Row>
              <ValidatedForm.Input
                icon={require('assets/mapPin.png')}
                name={'departing'}
                placeholder={'Departing'}
                autoCapitalize={'characters'}
                length={l => (l > 2)}
              />
            </ValidatedForm.Row>
            <ValidatedForm.Submit>
              {'Find booking'}
            </ValidatedForm.Submit>
          </ValidatedForm>
        </Container>
      );

export default CheckIn;
