// @flow

import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import Button from '.';

describe('Button Atom', () => {
  it('renders correctly', () => {
    const component = renderer.create(
      <Button onPress={() => {}}>
        {'Test Button'}
      </Button>,
    );
    expect(component.toJSON()).toMatchSnapshot();
  });

  it('calls a function on press', () => {
    const mockFn = jest.fn(),
          component = renderer.create(
            <Button onPress={mockFn}>
              {'Test Button'}
            </Button>,
          ).getInstance();

    component.props.onPress();

    expect(mockFn).toHaveBeenCalled();
  });
});
