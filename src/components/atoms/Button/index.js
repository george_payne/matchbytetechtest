// @flow

import React, { Component } from 'react';
import type { Children } from 'react';
import {
  TouchableWithoutFeedback,
  Animated,
} from 'react-native';
import styled from 'styled-components/native';
import colors from 'constants/colors';

const ButtonBlock = styled.View`
        flex-direction: row;
        justify-content: center;
        align-items: center;
        padding-right: 4;
        padding-bottom: 4;
        margin: 6 3;
      `,
      ButtonText = styled(Animated.Text)`
        flex: 1;
        color: ${colors.offWhite};
        background-color: ${colors.darkGray};
        border-radius: 4;
        padding: 8;
        text-align: center;
        font-weight: bold;
        font-size: 18;
      `,
      ButtonBorder = styled.View`
        flex: 1;
        position: absolute;
        top: 4;
        right: 0;
        bottom: 0;
        left: 4;
        border-radius: 4;
        border: 1 solid ${colors.darkGray};
      `;

class Button extends Component {
  props: {|
    children?: Children,
    disabled?: boolean,
    onPress: Function,
  |}

  static defaultProps = {
    children: null,
    disabled: false,
  }

  state = {
    indent: new Animated.ValueXY(),
  }

  indent(to: [number, number]) {
    Animated.spring(
    this.state.indent,
    { toValue: { x: to[0], y: to[1] } },
    ).start();
  }

  render() {
    const { children, disabled, onPress } = this.props,
          { indent } = this.state;

    return (
      <TouchableWithoutFeedback
        disabled={disabled}
        onPress={onPress}
        onPressIn={() => this.indent([4, 4])}
        onPressOut={() => this.indent([0, 0])}
      >
        <ButtonBlock>
          <ButtonText style={{ transform: [{ translateX: indent.x }, { translateY: indent.y }] }}>
            { children }
          </ButtonText>
          <ButtonBorder />
        </ButtonBlock>
      </TouchableWithoutFeedback>
    );
  }
}

export default Button;
