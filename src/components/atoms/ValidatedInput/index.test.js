// @flow
/* eslint-disable no-underscore-dangle, one-var*/

import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import ValidatedInput from '.';

describe('ValidatedInput Atom', () => {
  it('renders correctly', () => {
    const component = renderer.create(
      <ValidatedInput />,
    );
    expect(component.toJSON()).toMatchSnapshot();
  });

  it('checks the length (if required)', () => {
    const c1 = renderer.create(
      <ValidatedInput
        length={2}
      />,
    ).getInstance();

    [
      ['', false],
      ['a', false],
      ['ab', true],
      ['abc', false],
      ['abcd', false],
    ].forEach((t) => {
      c1._updateValue(t[0]);
      expect(c1.isValid).toBe(t[1]);
    });

    const c2 = renderer.create(
      <ValidatedInput
        length={l => (l > 4)}
      />,
    ).getInstance();

    [
      ['', false],
      ['a', false],
      ['ab', false],
      ['abcd', false],
      ['abcde', true],
      ['abcdefghi', true],
      ['abcdefghijkl', true],
      ['abc', false],
    ].forEach((t) => {
      c2._updateValue(t[0]);
      expect(c2.isValid).toBe(t[1]);
    });

    const c3 = renderer.create(
      <ValidatedInput />,
    ).getInstance();

    [
      ['', true],
      ['a', true],
      ['ab', true],
      ['abcd', true],
      ['abcde', true],
      ['abcdefghi', true],
      ['abcdefghijkl', true],
      ['abc', true],
    ].forEach((t) => {
      c3._updateValue(t[0]);
      expect(c3.isValid).toBe(t[1]);
    });
  });

  it('checks against a regular expression (if required)', () => {
    // only numbers and letters
    const c1 = renderer.create(
      <ValidatedInput
        validator={/^[A-z0-9]+$/}
      />,
    ).getInstance();

    [
      ['', false],
      ['a2', true],
      ['a5b', true],
      ['a/bc', false],
      ['ab@[]d3cd', false],
      ['////z/5', false],
      ['ab@[]d3cd', false],
    ].forEach((t) => {
      c1._updateValue(t[0]);
      expect(c1.isValid).toBe(t[1]);
    });

    // only upper case letters
    const c2 = renderer.create(
      <ValidatedInput
        validator={/^[A-Z]+$/}
      />,
    ).getInstance();

    [
      ['', false],
      ['a', false],
      ['asadasdSSSb', false],
      ['CAPS', true],
      ['JSJS8K', false],
      ['KKSK8j', false],
      ['SSIHY', true],
      ['ab/c', false],
    ].forEach((t) => {
      c2._updateValue(t[0]);
      expect(c2.isValid).toBe(t[1]);
    });

    // no validation
    const c3 = renderer.create(
      <ValidatedInput />,
    ).getInstance();

    [
      ['', true],
      ['a', true],
      ['a$$', true],
      ['5abcd', true],
      ['abr%cde', true],
      ['abcdefghi', true],
      ['abcde3455$fghijkl', true],
      ['abc', true],
    ].forEach((t) => {
      c3._updateValue(t[0]);
      expect(c3.isValid).toBe(t[1]);
    });
  });
});
