// @flow

import React, { Component } from 'react';
import styled from 'styled-components/native';
import colors from 'constants/colors';

const InputOuter = styled.View`
        flex: 1;
        margin: 6 4;
        position: relative;
        padding-right: 3;
        padding-bottom: 3;
      `,
      InputWrap = styled.View`
        flex-direction: row;
        justify-content: center;
        align-items: center;
        border-width: 2;
        border-style: solid;
        border-color: ${({ invalid }) => invalid ? colors.red : colors.lightGray};
        border-radius: 4;
        background-color: ${colors.white};
      `,
      InputField = styled.TextInput`
        flex: 1;
        color: ${colors.darkGray};
        padding: 8;
        text-align: left;
        font-size: 16;
      `,
      DropShadow = styled.TextInput`
        flex: 1;
        background-color: ${colors.shadowGray};
        position: absolute;
        top: 3;
        right: 0;
        bottom: 0;
        left: 3;
        border-radius: 4;
      `,
      Icon = styled.Image`
        width: 24;
        height: 24;
        margin-left: 6;
      `;

class ValidatedInput extends Component {
  props: {|
    length: (number => boolean) | number,
    validator: RegExp | false,
    icon: string,
    editable?: boolean,
    placeholder: string,
    autoFocus: boolean,
    autoCapitalize: 'none' | 'sentences' | 'words' | 'characters',
    keyboardType: 'default' | 'numeric' | 'email-address' | 'phone-pad',
    returnKeyType: '' | 'done' | 'go' | 'next' | 'search' |'send',
  |}

  static defaultProps = {
    // $FlowIssue https://github.com/facebook/flow/issues/1660
    length: l => (l <= Infinity),
    // $FlowIssue https://github.com/facebook/flow/issues/1660
    validator: false,
    icon: '',
    editable: true,
    placeholder: '',
    autoFocus: false,
    autoCapitalize: 'none',
    keyboardType: 'default',
    returnKeyType: '',
  }

  state = {
    value: '',
    displayValue: [],
    valid: true,
    displayValidity: false,
    returnKeyType: 'next',
  }

  isValid = false;
  value = '';

  _raw = null;

  _setValidity(valid: boolean) {
    this.setState({ valid });
    this.isValid = valid;
    return valid;
  }

  _updateValue(value: string) {
    this.validate(value, false);
    this.setState({ value });
    this.value = value;
  }

  validate(value: string = this.state.value, external: boolean = true) {
    const { length, validator } = this.props;

    if (external) {
      this.setState({
        displayValidity: true,
      });
    }

    if (
      (typeof length === 'function' && !length(value.length)) ||
      (typeof length === 'number' && value.length !== length)
    ) {
      if (external) this.focus();
      return this._setValidity(false);
    }

    if (validator && !validator.test(value)) {
      if (external) this.focus();
      return this._setValidity(false);
    }

    return this._setValidity(true);
  }

  focus() {
    if (this._raw) {
      this._raw.focus();
    }
  }

  setReturnKeyType(to: string) {
    this.setState({
      returnKeyType: to,
    });
  }

  render() {
    const { length, validator, icon, returnKeyType: rkt, ...inputProps } = this.props,
          { value, valid, displayValidity, returnKeyType } = this.state;

    return (
      <InputOuter>
        <DropShadow />
        <InputWrap invalid={displayValidity && !valid} >
          { !!icon &&
            <Icon source={icon} />
          }
          <InputField
            ref={(r) => { if (r) this._raw = r.root; }}
            value={value}
            onChangeText={v => this._updateValue(v)}
            underlineColorAndroid={'transparent'}
            maxLength={typeof length === 'number' ? length : null}
            placeholderTextColor={colors.darkGray}
            returnKeyType={rkt || returnKeyType}
            autoCorrect={false}
            blurOnSubmit={false}
            {...inputProps}
          />
        </InputWrap>
      </InputOuter>
    );
  }
}

export default ValidatedInput;
