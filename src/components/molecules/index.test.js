// @flow

import fs from 'fs';
import path from 'path';
import * as exported from '.';

describe('Molecules directory', () => {
  it('should should export all molecules', () => {
    fs
      .readdirSync(__dirname)
      .filter(file =>
        fs.statSync(path.join(__dirname, file)).isDirectory(),
      )
      .forEach(dir =>
        expect(exported[dir]).toBeDefined(),
      );
  });
});
