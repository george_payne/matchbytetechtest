// @flow

import React, { Component, cloneElement } from 'react';
import type { Children } from 'react';
import styled from 'styled-components/native';
import { ValidatedInput, Button } from 'atoms';

const FormOuter = styled.View`
        flex-direction: column;
        justify-content: center;
        align-items: center;
      `,
      FormRow = styled.View`
        flex-direction: row;
        justify-content: center;
        align-items: center;
      `;

class ValidatedForm extends Component {
  props: {|
    children: Children,
    submit: (values: {}) => void
  |}

  static Row = FormRow;
  static Input = ValidatedInput;
  static Submit = Button;

  componentDidMount() {
    const {
      _autoFocus: autoFocus,
      _inputs: inputs,
      _inputLookup: lookup,
    } = this;

    if (autoFocus) {
      lookup[inputs[0]].focus();
    }

    lookup[inputs[inputs.length - 1]].setReturnKeyType('done');
  }

  _inputs = [];
  _inputLookup = {};
  _submit = null;
  _autoFocus = true;


  tryToSubmit() {
    const {
      _inputs: inputs,
      _inputLookup: lookup,
    } = this;
    let valid = true;

    for (let i = 0; i < inputs.length; i++) {
      if (!lookup[inputs[i]].isValid) {
        valid = false;
        break;
      }
    }

    if (valid) {
      return this.submit();
    }

    inputs.reverse().forEach(input => lookup[input].validate());
  }

  submit() {
    const { submit } = this.props,
          {
            _inputs: inputs,
            _inputLookup: lookup,
          } = this,
          values = {};

    inputs.forEach((input) => { values[input] = lookup[input].value; });

    submit(values);
  }

  submitEditing(name: string) {
    const {
            _inputs: inputs,
            _inputLookup: lookup,
          } = this,
          inputIndex = inputs.indexOf(name);


    if (inputIndex !== inputs.length - 1) {
      lookup[inputs[inputIndex + 1]].focus();
    } else {
      this.tryToSubmit();
    }
  }

  renderChildren = (children: Children) => React.Children.map(children,
    (child) => {
      switch (child.type) {
      case (ValidatedForm.Row):
        return cloneElement(child, {
          children: this.renderChildren(child.props.children),
        });
      case (ValidatedForm.Input):
        if (child.props.autoFocus) {
          this._autoFocus = false;
        }
        return cloneElement(child, {
          ref: (r) => {
            this._inputs.push(r.props.name);
            this._inputLookup[r.props.name] = r;
          },
          onSubmitEditing: () => {
            this.submitEditing(child.props.name);
          },
        });
      case (ValidatedForm.Submit):
        this._submit = child;
        return cloneElement(child, {
          ref: (r) => { this._submit = r; },
          onPress: () => this.tryToSubmit(child.props.onPress),
        });
      default:
        return child;
      }
    },
  )

  render() {
    const { children } = this.props;
    return (
      <FormOuter>
        { this.renderChildren(children) }
      </FormOuter>
    );
  }
}

export default ValidatedForm;
