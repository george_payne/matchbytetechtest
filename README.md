# Matchbyte Tech Test

### Running the app

Install `app-release.apk` on your android device.

[Click here if you are unsure how.](http://www.wikihow.tech/Install-APK-Files-on-Android)


### Building / running the app locally

Standard React Native:

`npm install`

`react-native run-android`

[Click here for further information.](https://facebook.github.io/react-native/releases/0.43/docs/getting-started.html)

### Running Tests

`npm test`

------------------------------------------------------------------------
## General info

### iOS

I don't currently have access to a mac, so am unable to build / test on iOS. 
Feel free to give it a go, I haven't used any native modules so there is no reason why it shouldn't work, however, it is completely untested.


### Map Pin Icon 

I would rather use [`react-native-vector-icons`](https://github.com/oblador/react-native-vector-icons) for this, but for a single icon, and within the timescale, I decided it wasn't worth installing / setting up.

### Styled Components

I'd read that [Styled Components](https://github.com/styled-components/styled-components) had added React Native support, so thought I would try it out as a replacement for `StyleSheet`. I quite Liked it, much more natural writing in a css style to StyleSheets object syntax.

------------------------------------------------------------------------
## Screenshots

Clean  | Invalid Field | Submitted
------------- | ------------- | -------------
![Screenshot_20170424-011635.png](https://bitbucket.org/repo/KrrXyA9/images/319438635-Screenshot_20170424-011635.png)  | ![Screenshot_20170424-012008.png](https://bitbucket.org/repo/KrrXyA9/images/576182112-Screenshot_20170424-012008.png) | ![Screenshot_20170424-012017.png](https://bitbucket.org/repo/KrrXyA9/images/1675744361-Screenshot_20170424-012017.png)